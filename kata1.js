// Basketball points (13.01)
// You are counting points for a 
// basketball game, given the amount 
// of 2-pointers scored and 3-pointers 
// scored, find the final points for 
// the team and return that value.


function ballBuster(num1, num2){
    return num1 * 2 + num2 * 3;
}

console.log(ballBuster(1, 1))
console.log(ballBuster(7, 5))
console.log(ballBuster(38, 8))
console.log(ballBuster(0, 1))
console.log(ballBuster(0, 0))

// Thoughts: I should be more 
// descriptive with arguments
// and parameters. Also maybe 
// add the console.log
// inside the functions.