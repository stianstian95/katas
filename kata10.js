
//      (28.01)
// Caeser's Cipher
// Julius Caesar protected his confidential 
// information by encrypting it using a cipher. 
// Caesar's cipher (check Resources section for more info) 
// shifts each letter by a number of letters. 
// If the shift takes you past the end of the alphabet, 
// just rotate back to the front of the alphabet. 
// In the case of a rotation by 3, w, x, y and z would map to z, a, b and c.

// Create a function that takes a string s (text to be encrypted) 
// and an integer k (the rotation factor). 
// It should return an encrypted string.

// 26 letters

function isLetter(char) {
    if (char.match(/[a-zA-Z]/)) {
        return true
    }
    false
}

function caesarCipher(string, integer) {
    newString = ""
    for(i=0; i < string.length; i++){

        letter = string[i]

        if(isLetter(letter) == true) {
            unicode = string.charCodeAt(i) + integer

            while (unicode > 122) {
                unicode = (unicode - 122) + 96
            }
            newLetter = String.fromCharCode(unicode)
            newString += newLetter

        } else {
            newString += letter
        }
    }
    return newString
}

console.log(caesarCipher("middle-outz", 2))
console.log(caesarCipher("Always-Look-on-the-Bright-Side-of-Life", 5))
console.log(caesarCipher("A friend in need is a friend indeed", 20))

console.log(caesarCipher("okffng-qwvb", -2))
console.log(caesarCipher("Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj", -5))
console.log(caesarCipher("U zlcyhx ch hyyx cm u zlcyhx chxyyx", -20))