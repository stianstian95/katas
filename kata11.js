//      (02.02)
// Oddish vs. Evenish
// Create a function that determines whether a 
// number is Oddish or Evenish. A number is Oddish 
// if the sum of all of its digits is odd, and a 
// number is Evenish if the sum of all of its digits is even. 
// If a number is Oddish, return "Oddish". Otherwise, 
// return "Evenish".

function makeDigit(number) {

    stringArray = (number + '').split('')
    
    digitArray = stringArray.map((element) => {
        return parseInt(element)
    })
    return digitArray
}

function oddishOrEvenish(number) {

    digitArray = makeDigit(number)
    sum = 0

    for (i = 0; i < digitArray.length; i++) {
        sum += digitArray[i]
    }
    
    if(sum % 2 == 0) {
        return "Evenish, the sum was: " + sum
    } else {
        return "Oddish, the sum was: " + sum
    }
}

console.log(oddishOrEvenish(43))
console.log(oddishOrEvenish(99))
console.log(oddishOrEvenish(123))