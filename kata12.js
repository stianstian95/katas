
//      (03.02)
// Hidden Anagram

// Create a function that takes two strings. 
// The first string contains a sentence containing 
// the letters of the second string in a consecutive 
// sequence but in a different order. The hidden anagram 
// must contain all the letters, including duplicates, 
// from the second string in any order and must not contain
// any other alphabetic characters.
// Write a function to find the anagram of the second string
// embedded somewhere in the first string. You should ignore
// character case, any spaces, and punctuation marks and 
// return the anagram as a lower case string with no s
// paces or punctuation marks.

function doesContain(a, b) {
    firstString = a.split('').sort().join('')
    secondString = b.split('').sort().join('')

    if (firstString.includes(secondString)) {
        return true
    }
    return false
}

function hiddenAnagram(sentence, secret) {
    sentenceArray = sentence.toLowerCase().split("")
    secretArray = secret.toLowerCase().split("")
    anagram = []

    for (i = 0; i < sentenceArray.length; i++) {
        if (secretArray.includes(sentenceArray[i])) {
            anagram.push(sentenceArray[i])
            sentenceArray.splice(i, 1)

        }
    }

    return anagram.join("")
}

console.log(hiddenAnagram("An old west action hero actor", "Clint Eastwood"))
console.log(hiddenAnagram("Mr. Mojo Rising could be a song title", "Jim Morrison"))
console.log(hiddenAnagram("Banana? margaritas", "ANAGRAM"))
console.log(hiddenAnagram("D e b90it->?$ (c)a r...d,,#~", "bad credit"))
console.log(hiddenAnagram("Bright is the moon", "Bongo mirth"))
