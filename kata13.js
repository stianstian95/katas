//              (04.02)
// The sum of the squares of the first ten natural numbers is, 385
// The square of the sum of the first ten natural numbers is, 3025
// Hence the difference between the sum of the squares of the 
// first ten natural numbers and the square of the sum is 2640
// Find the difference between the sum of the squares of the first 
// one hundred natural numbers and the square of the sum.

function sumSquareDifference() {
    sumOfSquare = 0
    squareOfSum = 0
    ticker = 0

    while (ticker <= 100) {
        sumOfSquare += Math.pow(ticker, 2)
        squareOfSum += ticker
        ticker++ 
    }

    squareOfSum = Math.pow(squareOfSum, 2)
    return squareOfSum - sumOfSquare
}

console.log(sumSquareDifference())