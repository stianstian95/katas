// Double Letter Word Splitt (14.01)
// Write a function called splitOnDoubleLetter() 
// that receives a word, of type string, as an 
// argument. The function should split the word 
// where any double letter is found and return 
// an array of the split word. If no repeated 
// letters are found, simply return an empty array


function splitOnDoubleLetter(word) {
    const wordArray = Array.from(word)
    let newArray = []
   
    for (let i = 0; i < wordArray.length; i++) {
        if (wordArray[i] = wordArray[i+1]) {
            newArray.push(wordArray.slice(0, i));
            newArray.push(wordArray.slice(i));
        }
        return newArray;
    }

}

function splitOnDoubleLetter(word) {
    var wordArray = [];
    for(var i=0; i< word.length; i++) {
        var char = word[i];
        var charNext = char[i+1]
        if(char == charNext){
            word.push(' ')
        }
        return wordArray
    }
}

console.log(splitOnDoubleLetter('Letter'))

//      daniel somethings solution
function splitOnDoubleWord2(word) {
    if (typeof word === 'string') {
        let previousChar = null;
        let newWord = "";
        // Mark each double letter with a space
        for (nextChar of word) {
            if (previousChar === nextChar) newWord += " ";
            newWord += nextChar;
            previousChar = nextChar;
        }
        // split at space
        const words = newWord.split(" ");
        if (words.length > 1) return words;
        else {
            return [];
        }
    }
}
console.log(splitOnDoubleWord2('Letter'))

//      Kariannes løsning
function splitOnDoubleLetter(word) {
    const wordArray = [];
    let index = 0;
    for(let i=0; i< word.length; i++) {
        const char = word[i];
        const charNext = char[i+1];
        if(char == charNext){
            wordArray.push(word.slice(index, charNext));
            index = charNext;
        }
    }
    if (wordArray.length > 0) {
        wordArray.push(word.slice(index));
    }
    return wordArray;
}
console.log(splitOnDoubleLetter("Letter"))
console.log(splitOnDoubleLetter("‘Really"))
console.log(splitOnDoubleLetter("Happy"))
console.log(splitOnDoubleLetter("Shall"))
console.log(splitOnDoubleLetter("‘Tool"))
console.log(splitOnDoubleLetter("Mississippi"))
console.log(splitOnDoubleLetter("Easy"))