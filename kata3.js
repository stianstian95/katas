//      17.01
// The Classic fizzBuzzer
// Write a short program that prints each 
// number from 1 to n on a new line.
// For each multiple of 3, print "Fizz" instead of the number.
// For each multiple of 5, print "Buzz" instead of the number.
// For numbers which are multiples of both 3 and 5, print "FizzBuzz" 
// instead of the number.
//
// First solution:
function fizzBuzzer(num) {
    for(i=1; i<=num; i++){
        if (i % 3 == 0) {
            if (i % 5 == 0){
                console.log("FizzBuzz");
            }
            else {
                console.log("Fizz");
            }
        }
        else if (i % 5 == 0) {
            console.log("buzz");
        }
        else {
            console.log(i)
        }
    }
}

// second solution(added logic operator)
function fizzBuzzer2(num) {
    for (i=1; i <= num; i++) {
        if (i % 3 == 0 && i % 5 == 0) {
            console.log("FizzBuzz")
        } else if (i % 5 == 0) {
            console.log("Buzz")
        } else if (i % 3 == 0) {
            console.log("Fizz")
        } else {
            console.log(i)
        }
    }
}

console.log(fizzBuzzer2(15));