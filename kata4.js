//      (18.01)
// Length of a Nested Array
// The .length property on an array will 
// return the number of elements in the array. 
// For example, the array below contains 2 elements:
// [1, [2, 3]]
// 2 elements, number 1 and array [2, 3]
// Suppose we instead wanted to know the total number 
// of non-nested items in the nested array. In the above 
// case, [1, [2, 3]] contains 3 non-nested items, 1, 2 and 3.
// Write a function that returns the total number of 
// non-nested items in a nested array.
// Examples
// getLength([1, [2, 3]]) ➞ 3
// getLength([1, [2, [3, 4]]]) ➞ 4
// getLength([1, [2, [3, [4, [5, 6]]]]]) ➞ 6
// getLength([1, [2], 1, [2], 1]) ➞ 5
// Notes
// An empty array should return 0.

// my first solution
function getLength2(array) {
    newArray = [];

    for (i=0; i < array.length; i++) {
        if(i.length > 1){
            for(p=0; p < i.length; i++) {
                newArray.push(p);
            }
        } else {
            newArray.push(i);
        }
    }
}

// my second solution
const getLength = (arr) => arr.flat(Infinity).length;


// an online solution
const getLength3 = (arr) => {
    let a = arr.flat(Infinity);
    //console.log(a);
    let b;
    if (arr.length = 0) {
      b = 0;
    } else b = a.length;
    return b;
};

//   josteins løsning
const getLenght4 = (iterable) => {
    let elementCount = 0
    if (iterable.length === 0){
        return elementCount
    }
    for (element of iterable){
        if (element.length === undefined){
            elementCount += 1 
        }
        else{
            elementCount += getLenght(element)
        }
    } 
    return elementCount
}

console.log(getLength([1, [2, 3]]));
console.log(getLength([1, [2, [3, 4]]]));
console.log(getLength([1, [2, [3, [4, [5, 6]]]]]));
console.log(getLength([1, [2], 1, [2], 1]));