//      19.05
// Create a function that takes a number 
// as an argument. Add up all the numbers 
// from 1 to the number you passed to the 
// function. For example, if the input is 4 
// then your function should return 10 
// because 1 + 2 + 3 + 4 = 10.
//
// first try
function addUp(num) {
    let added = 0;
    for(i=0; i < num; i++){
        p = i
        added += p
    }
    return added;
}

// second try
function adderUpper(num) {
    let toBeReturned = 0;
    for (i=num; i > 0; i--){
        toBeReturned += i;
    }
    return toBeReturned;
}

console.log(adderUpper(4))
console.log(adderUpper(13))
console.log(adderUpper(600))

// Create a function that takes voltage 
// and current and returns the calculated power.
function circuitPower(voltage, current) {
    return voltage * current;
}

console.log(circuitPower(230, 10))
console.log(circuitPower(110, 3))
console.log(circuitPower(480, 20))

// Create a function that takes a number x and 
// a character y ("m" for male, "f" for female), 
// and returns the name of an ancestor (m/f) 
// or descendant (m/f).
function generationCalc(x, y) {
    if (y=="f") {
        switch (x) {
            case -3:
                console.log("great grandmother");
                break;
            case -2:
                console.log("grandmother");
                break;
            case -1:
                console.log("mother");
                break;
            case 1:
                console.log("daugther");
                break;
            case 2:
                console.log("granddaughter")
                break;
            case 3:
                console.log("great granddaugther");
                break;
            default:
                console.log("me!");
                break;
        }
    } else {
        switch (x) {
            case -3:
                console.log("great grandfather");
                break;
            case -2:
                console.log("grandfather");
                break;
            case -1:
                console.log("father");
                break;
            case 1:
                console.log("son");
                break;
            case 2:
                console.log("grandson")
                break;
            case 3:
                console.log("great grandson");
                break;
            default:
                console.log("me!");
                break;
        }
    }
}

console.log(generationCalc(2, "f"))
console.log(generationCalc(-3, "m"))
console.log(generationCalc(1, "f"))
console.log(generationCalc(0, "f"))