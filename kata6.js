//
//              (20.01)
//
// function correctTitle(name) {
//     toLowerCase = name.toLowerCase();
//     splitOnSpace = toLowerCase.split(/[\s,]+/);

//     for (let word of splitOnSpace) {
//         if(word === 'and' || word === 'the' || word === 'of' || word === 'in'){
//             // do nothing
//         } else {
//            return word.charAt(0).toUpperCase() + word.slice(1);
//         }
//     }
//     return splitOnSpace;
// }

function correctTitle2(name) {
    toLowerCase = name.toLowerCase();
    //badWords = [" ", ","];
    splitArray = toLowerCase.split(/[\s,]+/);

    for(var i = 0; i < splitArray.length; i++) {
        if(splitArray[i] === 'and' || splitArray[i] == 'the' || splitArray[i] == 'of' || splitArray[i] == 'in') {
            // do nothing jon snow
        } else {
        splitArray[i] = splitArray[i].charAt(0).toUpperCase() + splitArray[i].substr(1);
        }
    }
    return splitArray.join(" ") + ".";
}

console.log(correctTitle2("jOn SnoW, kINg IN thE noRth"));
console.log(correctTitle2("sansa stark,lady of winterfell"));
console.log(correctTitle2("TYRION LANNISTER, HAND OF THE QUEEN."));