//  (21.01)
// How Many Shuffles?
// An out-shuffle, also known as an out Faro shuffle 
// or a perfect shuffle, is a controlled method for 
// shuffling playing cards. It is performed by 
// plitting the deck into two equal halves and 
// interleaving them together perfectly, with the 
// condition that the top card of the deck remains in place.

function makeDeck(num) {
    let newDeck = [];
    for (let i = 0; i <= num; i++) {
        newDeck.push(i)
    }
    newDeck.shift();
    return newDeck;
}

function shuffleCount(num) {
    deck = makeDeck(num);
    firstNum = deck[0];
    deck.shift();
    const half1 = deck.slice(0, num / 2);
    const half2 = deck.slice(num / 2, num);
    newDeck = [];

    half2.forEach((card, index) => newDeck.push(card, half1[index]));

    deck.unshift(firstNum)
    return newDeck;
}


console.log(shuffleCount(8))