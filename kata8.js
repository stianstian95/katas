//         (26.01)
// Number of Boomerangs

// A boomerang is a V-shaped sequence that is either upright 
// or upside down. Specifically, a boomerang can be defined as: 
// sub-array of length 3, with the first and last digits being 
// the same and the middle digit being different.

// Some boomerang examples:

// [3, 7, 3], [1, -1, 1], [5, 6, 5]
// Create a function that returns the total number of  
// boomerangs in an array. 

// To illustrate:
// [3, 7, 3, 2, 1, 5, 1, 2, 2, -2, 2]
// // 3 boomerangs in this sequence: 
// [3, 7, 3], [1, 5, 1], [2, -2, 2]
// Be aware that boomerangs can overlap, like so:
// [1, 7, 1, 7, 1, 7, 1]
// // 5 boomerangs (from left to right):
// [1, 7, 1], [7, 1, 7], [1, 7, 1], [7, 1, 7], and [1, 7, 1]
// Examples
// countBoomerangs([9, 5, 9, 5, 1, 1, 1]) ➞ 2
// countBoomerangs([5, 6, 6, 7, 6, 3, 9]) ➞ 1
// countBoomerangs([4, 4, 4, 9, 9, 9, 9]) ➞ 0
// countBoomerangs([1, 7, 1, 7, 1, 7, 1]) ➞ 5
// Notes
// [5, 5, 5] (triple identical digits) is NOT considered a 
// boomerang because the middle digit is identical to the first and last

// function isFull(array) {
//     const emptyArray = []
//     if(array.length == 3) {
//         return true
//     }
//     return false
// }

// function isBoomerang(subArray) {

// }

// function countBoomerangs(array) {
//     let trio = []
//     counter = 0

//     for(i = 0; i < array.length; i++) {

//         if(trio.length == 3 || trio[0] == trio[2]) {
//             counter += 1
//             trio = []
//         } else {
//             element = array.shift()
//             trio.push(element)
//         }
//     }
//     return counter
// }

// console.log(countBoomerangs([9, 5, 9, 5, 1, 1, 1]))
// console.log(countBoomerangs([5, 6, 6, 7, 6, 3, 9]))
// console.log(countBoomerangs([4, 4, 4, 9, 9, 9, 9]))
// console.log(countBoomerangs([1, 7, 1, 7, 1, 7, 1]))

const countBoomerangs = (array) => {
    let boomerHolder = [];
    let boomerangs = [];

    for (let i = 0; i < array.length; i++) {

      if (array[i] === array[i + 2] && array[i + 1] !== array[i]) {

        (boomerHolder.push(array[i]));
        (boomerHolder.push(array[i + 1]));
        (boomerHolder.push(array[i + 2]));

        boomerangs.push(boomerHolder);
        console.log(boomerangs)
        boomerHolder = [];
      }
    }

    let boomerangCount = boomerangs.length;
    return boomerangCount;

  };
  
console.log(countBoomerangs([9, 5, 9, 5, 1, 1, 1]))
console.log(countBoomerangs([5, 6, 6, 7, 6, 3, 9]))
console.log(countBoomerangs([4, 4, 4, 9, 9, 9, 9]))
console.log(countBoomerangs([1, 7, 1, 7, 1, 7, 1]))