
//     (27.01)
// Tic Tac Toe

// Given a 3x3 matrix of a completed tic-tac-toe game, 
// create a function that returns whether the game is a 
// win for "X", "O", or a "Draw", where "X" and "O" 
// represent themselves on the matrix, and "E" represents 
// an empty spot.

function isSubset(array1, array2) {
    return array2.every(function (element) {
        return array1.includes(element)
    });
}

function ticTacToe(game) {
    xWon = false
    oWon = false

    const winningConditions = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ]

    for (let i = 0; i <= 8; i++) {
        const winningCondition = winningConditions[i];
        let a = isSubset(game, winningConditions)
    }
    return result
}

console.log(ticTacToe([

    ["X", "O", "X"],
  
    ["O", "X",  "O"],
  
    ["O", "X",  "X"]
  
  ]))